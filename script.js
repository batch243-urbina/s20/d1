// console.log(`Last session of the week!`);

// WHILE LOOP
// takes in an expression/condition
// expressions are any unit of code that can be evaluated to a value
// if the condition evaluates to be true, the statements inside the code block will be executed
// a loop will iterate a certain number of times until an expression/condition is met

/* 
Syntax: 
	while(expression/condition){
		statements;
		iteration;
	}
*/

let count = 5;

while (count !== 0) {
  console.log(`While: ${count}`);

  // decrement
  count--;
  console.log(`${count}`);
}

// DO WHILE LOOP
// guarantee that the code will be executed atleast once
/* 
	Syntax: 
	do{
		statement;
		iteration;
	}
	while(expression/condition)
*/

let number = `Give me a number`;

do {
  console.log(`Do while: ${number}`);

  number++;
} while (number < 10);

// FOR LOOP
// more flexible than while and do-while
// 1. Initialization
// 2. expression/condition
// 3. final expression -indicates how to advance the loop

/*
	Syntax: for (initialization; expression/condition; finalExpression) {
		statement / statements;
	}
*/

// we will create a loop that will start from 0 and end at 20
// every iteration of the loop, the value of the count will be checked if it is equal or less than 20
// if the value of the count is less than of equal to 20, the statement inside the loop will execute

for (let count = 15; count <= 20; count++) {
  console.log(`Count is: ${count}`);
}

let myString = "Alex";

console.log(myString.length);
console.log(myString.indexOf("e"));

for (let i = 0; i < myString.length; i++) {
  console.log(`${myString[i]}`);
}

let numA = 15;
for (let i = 0; i <= 10; i++) {
  let exponential = numA ** i;
  console.log(exponential);
}

let myName = "Alex";
// loop that will print out the letters of the name individually and print out the number 3 instead when the letter to be printed out is a vowel
for (let i = 0; i < myName.length; i++) {
  if (
    myName[i].toLowerCase() === "a" ||
    myName[i].toLowerCase() === "e" ||
    myName[i].toLowerCase() === "i" ||
    myName[i].toLowerCase() === "o" ||
    myName[i].toLowerCase() === "u"
  ) {
    console.log(3);
  } else {
    console.log(`${myName[i]}`);
  }
}

// Continue and Break
// Continue statements go to next iteration without finishing the execution of all statements
// Break statements used to terminate the current loop once a match has been found

// Creates a loop that if the count value is divided by 2 and the remainder is 0, it will print the number and continue to the next iteration of the loop
//     - How this For Loop works:
//         1. The loop will start at 0 for the the value of "count".
//         2. It will check if "count" is less than the or equal to 20.
//         3. The "if" statement will check if the remainder of the value of "count" divided by 2 is equal to 0 (e.g 0/2).
//         4. If the expression/condition of the "if" statement is "true" the loop will continue to the next iteration.
//         5. If the value of count is not equal to 0, the console will print the value of "count".
//         6. The second if statement will check if the value of "count" is greater than 10. (e.g. 0)
//         7. If the expression/condition of the second "if" statement is false the loop will proceed to the next iteration.
//         8. The value of "count" will be incremented by 1 (e.g. count = 1)
//         9. Then the loop will repeat steps 2 to 8 until the expression/condition of the loop is "false" or the condition of the second "if" statement

for (let count = 0; count <= 20; count++) {
  // if remainder is equal to 0
  if (count % 2 === 0) {
    continue;
  } else if (count > 10) {
    break;
  }
  console.log(`Continue and Break: ${count}`);
}

let nameAlex = "alexandro";

for (let i = 0; i < nameAlex.length; i++) {
  if (nameAlex[i].toLowerCase() === "a") {
    console.log(`Continue to next iteration`);
    continue;
  }

  if (nameAlex[i].toLowerCase() === "d") {
    console.log(`Console log before the break!`);
    break;
  }
  console.log(nameAlex[i]);
}
